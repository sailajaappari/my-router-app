import React, { useState, useEffect } from 'react';

const ListTodo = ({ todos }) => {
  //   const Todo = todos.map(todo => <li> {todo.text}</li>);

  return (
    <div>
      <ul>
        {todos.map((todo, i) => (
          <li key={i}> {todo.text}</li>
        ))}
      </ul>
    </div>
  );
};

const AddTodo = ({ todo, handleChange, handleClick }) => {
  return (
    <div>
      <span>
        <input
          type="text"
          value={todo.text}
          onChange={event => handleChange(event)}
        />
        <button onClick={() => handleClick()}>Add</button>
      </span>
    </div>
  );
};

export const TodosComp = () => {
  const [todos, setTodos] = useState([{ text: 'Learn Hooks' }]);
  const [todo, setTodo] = useState({ text: '' });
  useEffect(() => {
    console.log(todos[0].text);
  });
  useEffect(() => {
    console.log(todo.text);
  });
  const handleClick = () => {
    setTodos([...todos, todo]);
    setTodo({ text: '' });
  };
  const handleChange = event => setTodo({ text: event.target.value });

  return (
    <div>
      <AddTodo
        todo={todo}
        handleChange={handleChange}
        handleClick={handleClick}
      />
      <ListTodo todos={todos} />
    </div>
  );
};
