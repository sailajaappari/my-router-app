import React, { useState, useReducer } from 'react';

// Counter using useReduce hook
const initialState = { count: 0 };

const init = payload => payload;

const reducer = (state, action) => {
  switch (action.type) {
    case 'increment':
      return { count: state.count + action.step };
    case 'decrement':
      return { count: state.count - action.step };
    // case 'reset':
    //   return action.payload;
    case 'reset':
      return init(action.payload);
    default:
      return { count: state.count };
  }
};

export const ReduceHook = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <div>
      <p>Count: {state.count}</p>
      <button onClick={() => dispatch({ type: 'increment', step: 2 })}>
        +
      </button>
      <button onClick={() => dispatch({ type: 'decrement', step: 2 })}>
        -
      </button>
      <button
        onClick={() => dispatch({ type: 'reset', payload: initialState })}
      >
        Reset
      </button>
    </div>
  );
};

// Counter using useState hook
const Counter = props => (
  <div>
    <p>{props.count}</p>
    <button onClick={props.increment}>+</button>
    <button onClick={props.decrement}>-</button>
  </div>
);

export const SimpleHook = () => {
  const [count, setCount] = useState(0);
  //   useEffect(() => {
  //     // document.title = `You clicked ${count} times`;
  //     console.log(`You clicked ${count} times`);
  //   });
  const increment = () => setCount(count + 1);
  const decrement = () => setCount(count - 1);
  return <Counter count={count} increment={increment} decrement={decrement} />;
};
