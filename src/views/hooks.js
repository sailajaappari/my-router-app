import React from 'react';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import { SimpleHook, ReduceHook } from './counter_hooks';
import { TodosComp } from './todos_hooks';

export const HooksExample = () => (
  <Router>
    <div>
      <h1>Hooks</h1>
      <ul>
        <li>
          <Link exact to="/hooks/counter">
            Counter
          </Link>
        </li>
        <li>
          <Link exact to="/hooks/counter/useReducer">
            Counter useReducer
          </Link>
        </li>
        <li>
          <Link to="/hooks/todos">Todos </Link>
        </li>
      </ul>
      <hr />
      <Route exact path="/hooks/counter" component={SimpleHook} />
      <Route exact path="/hooks/counter/useReducer" component={ReduceHook} />
      <Route path="/hooks/todos" component={TodosComp} />
    </div>
  </Router>
);
